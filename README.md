# Docker Image with kubectl, kustomize, and flux-cli

This repository contains a Dockerfile to build a Docker image based on Alpine Linux that includes the following tools:

- kubectl: The Kubernetes command-line tool.
- kustomize: A tool for customizing Kubernetes configurations.
- flux-cli: The command-line tool for Flux, a GitOps tool for Kubernetes.

## Purpose

The purpose of this Docker image is to provide a lightweight environment with essential Kubernetes-related tools installed. It can be useful for Kubernetes developers, administrators, or anyone working with Kubernetes clusters.

## Building the Docker Image Locally

To build this Docker image locally, follow these steps:

   ```bash
   git clone git@gitlab.com:madcon/madcon-ci-tools.git
   cd madcon-ci-tools
   docker build -t madcon-ci-tools:latest .
   ```


