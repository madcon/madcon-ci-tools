# Use Alpine Linux as the base image
FROM alpine:latest

# Install required dependencies
RUN apk update && \
    apk add --no-cache curl bash && \
    rm -rf /var/cache/apk/*

# Install kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x kubectl && \
    mv kubectl /usr/local/bin/

# Install kustomize
RUN curl -LO "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" && \
    /bin/bash install_kustomize.sh /usr/local/bin && rm install_kustomize.sh

# Install flux-cli
RUN curl -LO https://fluxcd.io/install.sh && /bin/bash install.sh && rm install.sh

# Install skaffold
RUN curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64 && \
    install skaffold /usr/local/bin/

# Set the default shell for the container
SHELL ["/bin/bash", "-c"]

